import React, {Component} from 'react';
var PropTypes = React.PropTypes;

class PromptContainer extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: ''
        }

    }
    render() {
        return (
            <div>
                <h1>Some Header Text{this.state.username}
                    -{this.state.password}
                </h1>
                <form>
                    <input type="text" onChange={this.usernameChanged.bind(this)} value={this.state.username}/>
                    <input type="text" onChange={this.passwordChanged.bind(this)} value={this.state.password}/>
                </form>
            </div>
        )
    }
    usernameChanged(event) {
        this.setState({username: event.target.value});
    }
    passwordChanged(event) {
        this.setState({password: event.target.value});

    }
}

//The app state is jeff now.
export default class App extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <div onClick={this.clickedName.bind(this)}>
                <div>Hello {this.props.name}</div>
                <PromptContainer/>
            </div>
        )
    }
    clickedName() {
        console.log('hello');
    }
}
