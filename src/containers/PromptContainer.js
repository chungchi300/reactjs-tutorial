var React = require('react');
var Prompt = require('../components/Prompt');
var PropTypes = React.PropTypes;
class PromptContainer extends React.Component {

    constructor() {
        super();
        this.state = {
            username: ''
        };

    }
    handleUpdateUser(e) {
        this.setState({username: e.target.value});
    }
    handleSubmitUser(e) {
        e.preventDefault();
        var username = this.state.username;
        this.setState({username: ''});
        if (true) 
            console.log(this);
        if (this.props.routeParams.playerOne) {
            this.context.router.push({
                pathname: '/battle',
                query: {
                    playerOne: this.props.routeParams.playerOne,
                    playerTwo: this.state.username
                }
            })
        } else {

            this.context.router.push('/playerTwo/' + this.state.username)
        }
    }
    render() {
        let spreadProps = {
            onSubmitUser: this.handleSubmitUser.bind(this),
            onUpdateUser: this.handleUpdateUser.bind(this),
            header: this.props.route.header,
            username: this.state.username
        }
        return (<Prompt {...spreadProps}/>)
    }
}
PromptContainer.contextTypes = {
    router: React.PropTypes.object.isRequired
}

PromptContainer.propTypes = {
    sss: PropTypes.string.isRequired
}
module.exports = PromptContainer;
