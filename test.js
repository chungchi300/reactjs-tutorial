import test from 'ava';
class Car {
    run() {
        console.log(this.name + 'is running');
    }
}
class Bus extends Car {
    constructor() {
        super();
        this.name = 'bus';
    }
}
class Taxi extends Car {
    constructor() {
        super();

        this.name = 'taxi';
    }

}
test('foo', t => {
    t.pass();
});

test('bar', t => {
    let bus = new Taxi();
    bus.run();
    t.pass();
});
