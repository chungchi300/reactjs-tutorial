var React = require('react');
//All basic container
var Main = React.createClass({
    render: function() {
        return (
            <div className='main-container'>
                This is main {this.props.children}
            </div>
        )
    }
});

module.exports = Main;
